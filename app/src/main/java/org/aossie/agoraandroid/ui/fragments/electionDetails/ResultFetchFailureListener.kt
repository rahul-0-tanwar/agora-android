package org.aossie.agoraandroid.ui.fragments.electionDetails

interface ResultFetchFailureListener {
  fun onResultFetchMessage(messageRef: Int)
}