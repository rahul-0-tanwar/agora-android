package org.aossie.agoraandroid.data.network.responses

data class AuthToken(
  val token: String?,
  val expiresOn: String?
)