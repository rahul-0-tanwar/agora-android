package org.aossie.agoraandroid.data.db.model

data class Ballot (
  val voteBallot: String ?= null,
  val hash:String ?= null
)