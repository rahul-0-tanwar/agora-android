package org.aossie.agoraandroid.data.db.model

data class VoterList(
  val name: String? = null,
  val hash: String? = null
)